import React from 'react';
import './Number.css';

const Number = props => {
    return(
        <div className="Number">
            <span>{props.number}</span>
        </div>
    )
};

export default Number;