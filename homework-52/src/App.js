import React, { Component } from 'react';
import './App.css';
import Number from './Number';
import { rand } from './random';

class App extends Component {

    state = {
        numbers: [0, 0, 0, 0, 0]
    };

    handleClick = () => {
        let randomNumbers = [];
        let count = 5;
        while(count > 0) {
            const randomNumber = rand(5, 36);
            if (randomNumbers.indexOf(randomNumber) === -1) {
                randomNumbers.push(randomNumber);
                count--;
            }
        }
        randomNumbers.sort((a, b) => a > b);
        this.setState({numbers: randomNumbers})
    };


    render() {
        return (
            <div className="App">

                <button id="new-numbers" onClick={this.handleClick}>New numbers</button>

                <Number number={this.state.numbers[0]}/>
                <Number number={this.state.numbers[1]}/>
                <Number number={this.state.numbers[2]}/>
                <Number number={this.state.numbers[3]}/>
                <Number number={this.state.numbers[4]}/>
            </div>
        );
    }
}

export default App;
